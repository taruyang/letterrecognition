#ifndef BACKPROPAGATION_H
#define BACKPROPAGATION_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include <QDebug>

//-----------------------
//file manipulation
#include <QFile>
#include <QTextStream>
#include <QDataStream>
//-----------------------

#include "globalVariables.h"

class Backpropagation
{
public:
   Backpropagation(ActFunc actHidden_ = Sigmoid, ActFunc actOut_ = Sigmoid);
   ~Backpropagation();
   /* Initialization */
   void initialise();
   void initTest();
   /* Related with weights */
   void saveWeights(QString fileName);
   void loadWeights(QString fileName);
   void assignRandomWeights( void );
   /* Propagation calculation */
   void feedForward( );
   void backPropagate();
   double actFunc( double val );
   double actFuncDerivative( double val );
   int action( double *vector );
   /* Network training */
   double trainNetwork(int NUMBER_OF_DESIRED_EPOCHS);
   double* testNetwork(LetterStructure testPattern);
   /* Performance */
   double getError_SSE();
   double getError_MSE();
   double getGoodClassificationPercent();
   /* Saving results */
   void saveTrainStatus(bool isLast);
   void saveConfusionMatrix();
private:
   /* Weight Structures */
   double wih1[INPUT_NEURONS+1][HIDDEN1_NEURONS];
   double wh1h2[HIDDEN1_NEURONS+1][HIDDEN2_NEURONS];
   double wh2o[HIDDEN2_NEURONS+1][OUTPUT_NEURONS];
   /* Confusion Matrix */
   int confusion[OUTPUT_NEURONS][OUTPUT_NEURONS];
   /* Activations */
   double inputs[INPUT_NEURONS];
   double hidden1[HIDDEN1_NEURONS];
   double hidden2[HIDDEN2_NEURONS];
   double target[OUTPUT_NEURONS];
   double actual[OUTPUT_NEURONS];
   /* Activation Function Type */
   ActFunc     actHidden;
   ActFunc     actOut;
   /* Unit Errors */
   double erro[OUTPUT_NEURONS];
   double errh1[HIDDEN1_NEURONS];
   double errh2[HIDDEN2_NEURONS];

   /* To save Train Status */
   QFile*      fileTs;
   //-----------------------------------------
   double err;
   int i, sample, iterations;
   int correctClassifications;
   double correctPercent;
   double MSE, SSE, totalEpochs;
   int sum;
};

#endif // BACKPROPAGATION_H
