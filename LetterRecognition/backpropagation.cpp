#include "backpropagation.h"

#define sqr(x)	((x) * (x))
#define RAND_WEIGHT	(((float)rand() / (float)RAND_MAX) - 0.5)
#define getSRand()	((float)rand() / (float)RAND_MAX)
#define getRand(x)      (int)((float)(x)*rand()/(RAND_MAX+1.0))

///////////////////////////////////////////////////////////////////
Backpropagation::Backpropagation(ActFunc actHidden_, ActFunc actOut_)
{
   actHidden = actHidden_;
   actOut = actOut_;
   fileTs = NULL;

   initialise();
}

Backpropagation::~Backpropagation()
{
   if(fileTs != NULL) {
      if(fileTs->isOpen())
         fileTs->close();
      delete fileTs;
   }
}

void Backpropagation::initialise()
{
   err=0;
   sample=0;
   correctClassifications = 0;
   sum = 0;
   correctPercent = 0.f;
   totalEpochs = 0;

   memset(confusion, 0, sizeof(confusion));

   /* Seed the random number generator */
   srand( time(NULL) );

   assignRandomWeights();

   if(fileTs == NULL)
      fileTs = new QFile(STATUS_FILE);
   else {
      if(fileTs->isOpen())
         fileTs->close();
   }

   fileTs->open(QIODevice::WriteOnly | QIODevice::Text);
   fileTs->write("EPOCH, MSE, SSE, PGC\n");
}

double Backpropagation::getError_SSE()
{
    return SSE;
}

double Backpropagation::getError_MSE()
{
    return MSE;
}

double Backpropagation::getGoodClassificationPercent()
{
   return correctPercent;
}

void Backpropagation::saveWeights(QString fileName)
{
   int out, hid, inp;

   QFile file3(fileName);
   file3.open(QIODevice::WriteOnly | QIODevice::Text);

   QTextStream out3(&file3);

   char tempBuffer3[80];
   QByteArray temp3;

   //----------------------------------------------
   // weights
   //
   qDebug() << "updating weights... to " << fileName;
   qDebug() << "OUTPUT_NEURONS = " << OUTPUT_NEURONS;
   qDebug() << "HIDDEN2_NEURONS = " << HIDDEN2_NEURONS;
   qDebug() << "HIDDEN1_NEURONS = " << HIDDEN1_NEURONS;
   qDebug() << "INPUT_NEURONS = " << INPUT_NEURONS;

   // Update the weights for the output layer (step 4 for output cell)
   for (out = 0 ; out < OUTPUT_NEURONS ; out++)
   {
      temp3.clear();
      for (hid = 0 ; hid < HIDDEN2_NEURONS ; hid++)
      {
         ::sprintf(tempBuffer3,"%f,",wh2o[hid][out]);
         temp3.append(tempBuffer3);
      }

      // Update the Bias
      ::sprintf(tempBuffer3,"%f",wh2o[HIDDEN2_NEURONS][out]);
      temp3.append(tempBuffer3);
      temp3.append("\n");
      out3 << temp3;
   }

   for (out = 0 ; out < HIDDEN2_NEURONS ; out++)
   {
      temp3.clear();
      for (hid = 0 ; hid < HIDDEN1_NEURONS ; hid++)
      {
         ::sprintf(tempBuffer3,"%f,",wh1h2[hid][out]);
         temp3.append(tempBuffer3);
      }

      // Update the Bias
      ::sprintf(tempBuffer3,"%f",wh1h2[HIDDEN1_NEURONS][out]);
      temp3.append(tempBuffer3);
      temp3.append("\n");
      out3 << temp3;
   }

   // Update the weights for the hidden layer (step 4 for hidden cell)
   for (hid = 0 ; hid < HIDDEN1_NEURONS ; hid++)
   {
      temp3.clear();
      for (inp = 0 ; inp < INPUT_NEURONS ; inp++)
      {
         ::sprintf(tempBuffer3,"%.8f,",wih1[inp][hid]);
         temp3.append(tempBuffer3);
      }

      // Update the Bias
      ::sprintf(tempBuffer3,"%.8f",wih1[INPUT_NEURONS][hid]);
      temp3.append(tempBuffer3);
      temp3.append("\n");
      out3 << temp3;
   }

   file3.close();
   qDebug() << "Weights saved to file.";
}

void Backpropagation::loadWeights(QString fileName)
{
   int out, hid, inp;

   QFile file3(fileName);
   file3.open(QIODevice::ReadOnly | QIODevice::Text);

   if(!file3.exists())
   {
      qDebug() << "Backpropagation::loadWeights-file does not exist!";
      return;
   }

   QTextStream in(&file3);

   char tChar;
   QByteArray temp3;

   //----------------------------------------------
   // weights
   //
   QString strLine;

   qDebug() << "loading weights...from " << fileName;
   qDebug() << "OUTPUT_NEURONS = " << OUTPUT_NEURONS;
   qDebug() << "HIDDEN1_NEURONS = " << HIDDEN1_NEURONS;
   qDebug() << "HIDDEN2_NEURONS = " << HIDDEN2_NEURONS;
   qDebug() << "INPUT_NEURONS = " << INPUT_NEURONS;

   // Update the weights for the output layer (step 4 for output cell)
   for (out = 0 ; out < OUTPUT_NEURONS ; out++)
   {
      strLine = in.readLine();
      QTextStream streamLine(&strLine);

      for (hid = 0 ; hid < HIDDEN2_NEURONS ; hid++)
         streamLine >> wh2o[hid][out] >> tChar;

      // Update the Bias
      //---load------------------------------------
      streamLine >> wh2o[HIDDEN2_NEURONS][out];// >> tChar;
   }

   for (out = 0 ; out < HIDDEN2_NEURONS ; out++)
   {
      strLine = in.readLine();
      QTextStream streamLine(&strLine);

      for (hid = 0 ; hid < HIDDEN1_NEURONS ; hid++)
         streamLine >> wh1h2[hid][out] >> tChar;

      // Update the Bias
      //---load------------------------------------
      streamLine >> wh1h2[HIDDEN2_NEURONS][out];// >> tChar;
   }

   /* Update the weights for the hidden layer (step 4 for hidden cell) */
   for (hid = 0 ; hid < HIDDEN1_NEURONS ; hid++)
   {
      strLine = in.readLine();
      QTextStream streamLine(&strLine);

      for (inp = 0 ; inp < INPUT_NEURONS ; inp++)
         streamLine >> wih1[inp][hid] >> tChar;

      // Update the Bias
      //---load------------------------------------
      streamLine >> wih1[INPUT_NEURONS][hid];
   }

   file3.close();
   qDebug() << "Weights loaded.";
}

// Req: For each training epoch, record the Mean Squared Error and the Percentage of Good Classification in a text file.
void Backpropagation::saveTrainStatus(bool isLast)
{
   if(fileTs->isOpen() == false) {
      fileTs->open(QIODevice::Append | QIODevice::Text);
   }

   QTextStream outTs(fileTs);
   outTs << totalEpochs << "," << MSE << "," << SSE << "," << correctPercent << endl;

   // To enhance performance
   if(isLast)
      fileTs->close();
}

void Backpropagation::saveConfusionMatrix()
{
   qDebug() << "[BackProp] Save Confusion Matrix";

   QFile fileCm(CON_MAT_FILE);
   fileCm.open(QIODevice::WriteOnly | QIODevice::Text);
   QTextStream outCm(&fileCm);

   for(int j = 0; j < OUTPUT_NEURONS; j++) {
      for(int i = 0; i < OUTPUT_NEURONS; i++)
         outCm << confusion[j][i] << ((i == OUTPUT_NEURONS - 1) ? "": ",");
      outCm << endl;
   }

   fileCm.close();

   qDebug() << "[BackProp] Completed saving Confusion Matrix";
}

int Backpropagation::action( double *vector )
{
   int index, sel;
   double max;

   sel = 0;
   max = vector[sel];

   for (index = 1 ; index < OUTPUT_NEURONS ; index++)
   {
      if (vector[index] > max)
      {
         max = vector[index];
         sel = index;
      }
   }

   return( sel );
}


void Backpropagation::initTest()
{
   err=0;
   sample=0;
   correctClassifications = 0;
   sum = 0;
   correctPercent = 0.f;
   totalEpochs = 0;
   memset(confusion, 0, sizeof(confusion));
}

double* Backpropagation::testNetwork(LetterStructure testPattern)
{
   //retrieve input patterns
   for(int j=0; j < INPUT_NEURONS; j++)
      inputs[j] = testPattern.f[j];

   for(int i=0; i < OUTPUT_NEURONS; i++)
      target[i] = testPattern.outputs[i];

   feedForward();

   // Squared Error
   for(int k=0; k < OUTPUT_NEURONS; k++)
      err += sqr((target[k] - actual[k]) );

   // Check whether the actual output is the expected output and count the number of correct Classification.
   int targetOut = testPattern.symbolIdx;
   int actualOut = action(actual);
   if (targetOut == actualOut)
      correctClassifications++;

   confusion[actualOut][targetOut]++;

   sample++;

   MSE = err / (double)sample;
   SSE = err / 2.0f;
   correctPercent = ((double)correctClassifications/(double)sample) * 100.f;

   return actual;
}

double Backpropagation::trainNetwork(int NUMBER_OF_DESIRED_EPOCHS)
{
   if(!patternsLoadedFromFile)
   {
      qDebug() << "unable to train network, no training patterns loaded.";
      return -999.99;
   }

   double accumulatedErr=0.0;
   int epochs=0;
   err = 0.0; MSE = 0.0; SSE = 0.0;
   sample = 0; correctClassifications = 0;
   memset(confusion, 0, sizeof(confusion));

   while (true)
   {
      if (sample == NUMBER_OF_TRAINING_PATTERNS)
      {
         MSE = err / (double)sample;
         SSE = err / 2.0f;
         correctPercent = ((double)correctClassifications/(double)sample) * 100.f;
         epochs++;totalEpochs++;
         qDebug() << totalEpochs << MSE << SSE << correctClassifications << sample << correctPercent;
         sample = 0; err = 0.0;
         correctClassifications = 0;
      }

      if(epochs >= NUMBER_OF_DESIRED_EPOCHS)
         break;

      //retrieve input patterns
      for(int j=0; j < INPUT_NEURONS; j++)
         inputs[j] = letters[sample].f[j];

      for(int i=0; i < OUTPUT_NEURONS; i++)
         target[i] = letters[sample].outputs[i];

      feedForward();

      backPropagate();

      // Mean Squared Error
      for (int k = 0 ; k < OUTPUT_NEURONS ; k++)
         err += sqr((target[k] - actual[k]));

      // Check whether the actual output is the expected output and count the number of correct Classification.
      int targetOut = letters[sample].symbolIdx;
      int actualOut = action(actual);
      if (targetOut == actualOut)
         correctClassifications++;

      // makes confusion matrix
      confusion[actualOut][targetOut]++;

      accumulatedErr = accumulatedErr + err;

      sample++;
   }

   return accumulatedErr;
}

/*
 *  assignRandomWeights()
 *
 *  Assign a set of random weights to the network.
 *
 */
void Backpropagation::assignRandomWeights( void )
{
   int hid, inp, out;

   for (inp = 0 ; inp < INPUT_NEURONS+1 ; inp++)
      for (hid = 0 ; hid < HIDDEN1_NEURONS ; hid++)
         wih1[inp][hid] = RAND_WEIGHT;

   for (inp = 0 ; inp < HIDDEN1_NEURONS+1 ; inp++)
      for (hid = 0 ; hid < HIDDEN2_NEURONS ; hid++)
         wh1h2[inp][hid] = RAND_WEIGHT;

   for (hid = 0 ; hid < HIDDEN2_NEURONS+1 ; hid++)
      for (out = 0 ; out < OUTPUT_NEURONS ; out++)
         wh2o[hid][out] = RAND_WEIGHT;
}

/*
 *  actFunc()
 *
 *  Calculate and return the active function of the val argument.
 *
 */
double Backpropagation::actFunc( double val )
{
   if(actHidden == ReLU)
      return (val > 0.f) ? val : 0;
   else if(actHidden == Sigmoid)
      return (1.0 / (1.0 + exp(-val)));
   else // tanh
      return tanh(val);
}

/*
 *  actFuncDerivative()
 *
 *  Calculate and return the derivative of the active function for the val argument.
 *
 */
double Backpropagation::actFuncDerivative( double val )
{
   if(actHidden == ReLU)
      return (val > 0.f) ? 1.f : 0.f;
   else if(actHidden == Sigmoid)
      return ( val * (1.0 - val) );
   else // tanh
      return 1- val * val;
}

/*
 *  feedForward()
 *
 *  Feedforward the inputs of the neural network to the outputs.
 *
 */
void Backpropagation::feedForward( )
{
   int inp, hid, out;
   double sum;

   /* Calculate input to hidden layer */
   for (hid = 0 ; hid < HIDDEN1_NEURONS ; hid++)
   {
      sum = 0.0;
      for (inp = 0 ; inp < INPUT_NEURONS ; inp++)
         sum += inputs[inp] * wih1[inp][hid];

      /* Add in Bias */
      sum += wih1[INPUT_NEURONS][hid];

      hidden1[hid] = actFunc( sum );
   }

   for (hid = 0 ; hid < HIDDEN2_NEURONS ; hid++)
   {
      sum = 0.0;
      for (inp = 0 ; inp < HIDDEN1_NEURONS ; inp++)
         sum += hidden1[inp] * wh1h2[inp][hid];

      /* Add in Bias */
      sum += wh1h2[HIDDEN1_NEURONS][hid];

      hidden2[hid] = actFunc( sum );
   }

   double totalSum = 0.f;

   /* Calculate the hidden to output layer */
   for (out = 0 ; out < OUTPUT_NEURONS ; out++)
   {
      sum = 0.0;
      for (hid = 0 ; hid < HIDDEN2_NEURONS ; hid++)
         sum += hidden2[hid] * wh2o[hid][out];

      /* Add in Bias */
      sum += wh2o[HIDDEN2_NEURONS][out];

      // Softmax activation function
      if(actOut == Softmax)
      {
         actual[out] = exp(sum);
         totalSum += actual[out];
      }
      else
        actual[out] = actFunc( sum );
   }

   // Softmax activation function
   if(actOut == Softmax)
      for (out = 0 ; out < OUTPUT_NEURONS ; out++)
         actual[out] /= totalSum;
}

/*
 *  backPropagate()
 *
 *  Backpropagate the error through the network.
 *
 */
void Backpropagation::backPropagate( void )
{
   int inp, hid, out;

   /* Calculate the output layer error (step 3 for output cell) */
   if(actOut == Softmax) {
      for (out = 0 ; out < OUTPUT_NEURONS ; out++)
         erro[out] = (actual[out] - target[out]);
   }
   else if (actOut == Sigmoid) {
      for (out = 0 ; out < OUTPUT_NEURONS ; out++)
         erro[out] = (target[out] - actual[out]) * actFuncDerivative( actual[out] );
   }

   /* Calculate the hidden layer error (step 3 for hidden cell) */
   for (hid = 0 ; hid < HIDDEN2_NEURONS ; hid++)
   {
      errh2[hid] = 0.0;
      for (out = 0 ; out < OUTPUT_NEURONS ; out++)
         errh2[hid] += erro[out] * wh2o[hid][out];

      errh2[hid] *= actFuncDerivative( hidden2[hid] );
   }

   for (hid = 0 ; hid < HIDDEN1_NEURONS ; hid++)
   {
      errh1[hid] = 0.0;
      for (out = 0 ; out < HIDDEN2_NEURONS ; out++)
         errh1[hid] += errh2[out] * wh1h2[hid][out];

      errh1[hid] *= actFuncDerivative( hidden1[hid] );
   }

   /* Update the weights for the output layer (step 4 for output cell) */
   for (out = 0 ; out < OUTPUT_NEURONS ; out++)
   {
      for (hid = 0 ; hid < HIDDEN2_NEURONS ; hid++)
         wh2o[hid][out] -= (LEARNING_RATE * erro[out] * hidden2[hid]);

      /* Update the Bias */
      wh2o[HIDDEN2_NEURONS][out] -= (LEARNING_RATE * erro[out]);
   }

   for (out = 0 ; out < HIDDEN2_NEURONS ; out++)
   {
      for (hid = 0 ; hid < HIDDEN1_NEURONS ; hid++)
         wh1h2[hid][out] -= (LEARNING_RATE * errh2[out] * hidden1[hid]);

      /* Update the Bias */
      wh1h2[HIDDEN1_NEURONS][out] -= (LEARNING_RATE * errh2[out]);
   }

   /* Update the weights for the hidden layer (step 4 for hidden cell) */
   for (hid = 0 ; hid < HIDDEN1_NEURONS ; hid++)
   {
      for (inp = 0 ; inp < INPUT_NEURONS ; inp++)
         wih1[inp][hid] -= (LEARNING_RATE * errh1[hid] * inputs[inp]);

      /* Update the Bias */
      wih1[INPUT_NEURONS][hid] -= (LEARNING_RATE * errh1[hid]);
   }
}
