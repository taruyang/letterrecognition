#ifndef GLOBALVARIABLES_H
#define GLOBALVARIABLES_H

//------------------------------------------

#define INPUT_NEURONS		16
#define HIDDEN1_NEURONS		100
#define HIDDEN2_NEURONS		100
#define OUTPUT_NEURONS		26

//------------------------------------------
#define STATUS_FILE        "status.csv"
#define CON_MAT_FILE        "confusion.csv"

enum ActFunc {ReLU, Sigmoid, Tanh, Softmax};

struct LetterStructure{
    int symbolIdx;
    int outputs[OUTPUT_NEURONS];
    double f[INPUT_NEURONS];
};

extern LetterStructure letters[20001];
extern LetterStructure testPattern;

//extern int NUMBER_OF_PATTERNS;
const int NUMBER_OF_PATTERNS = 20000;
const int NUMBER_OF_TRAINING_PATTERNS = 16000;
const int NUMBER_OF_TEST_PATTERNS = 4000;
const double TARGET_SSE = 0.0017f;
const double TARGET_GOOD_CLAASIFICATION = 99.f;
extern bool patternsLoadedFromFile;
extern int MAX_EPOCHS;
extern double LEARNING_RATE;

#endif // GLOBALVARIABLES_H
