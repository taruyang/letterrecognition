#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "globalVariables.h"
#include <algorithm>

//--------------------------------------

LetterStructure letters[20001];
LetterStructure testPattern;

bool patternsLoadedFromFile;
int MAX_EPOCHS;
double LEARNING_RATE;
bool  saveFile = true;
//int NUMBER_OF_PATTERNS;

///////////////////////////////////////////////////////

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //---------------------------------------
    //initialisation of global variables
    //

    //NUMBER_OF_PATTERNS = 20000;

    LEARNING_RATE=0.2;
    patternsLoadedFromFile = false;
    MAX_EPOCHS = 50;

    bp = new Backpropagation(ReLU, Softmax);

    //---------------------------------------
    //initialise widgets

    ui->spinBox_training_Epochs->setValue(MAX_EPOCHS);
    ui->horizScrollBar_LearningRate->setValue(int(LEARNING_RATE*100));
}

MainWindow::~MainWindow()
{   delete ui;
   delete bp;
}

void MainWindow::on_pushButton_Read_File_clicked()
{
   qDebug() << "\nReading file...";
   QFile file("./complete_data_set.txt");
   file.open(QIODevice::ReadOnly | QIODevice::Text);

   if(!file.exists()){
      patternsLoadedFromFile=false;

      qDebug() << "Data file does not exist!";
      return;
   }

   QTextStream in(&file);

   char t;
   char characterSymbol;
   QString line;

   int counterForLetter[OUTPUT_NEURONS] = {0,};
   QString lineOfData;
   QString msg;
   int i=0;
   //while(i < NUMBER_OF_TRAINING_PATTERNS){
   while(i < NUMBER_OF_PATTERNS){
      //e.g. T,2,8,3,5,1,8,13,0,6,6,10,8,0,8,0,8
      memset(&letters[i], 0, sizeof(LetterStructure));
      in >> characterSymbol >> t >> letters[i].f[0] >> t >>  letters[i].f[1] >> t >>  letters[i].f[2] >> t >>  letters[i].f[3] >> t >>  letters[i].f[4] >> t >>  letters[i].f[5] >> t >>  letters[i].f[6] >> t >>  letters[i].f[7] >> t >>  letters[i].f[8] >> t >>  letters[i].f[9] >> t >>  letters[i].f[10] >> t >>  letters[i].f[11] >> t >> letters[i].f[12] >> t >> letters[i].f[13] >> t >> letters[i].f[14] >> t >> letters[i].f[15];
      line = in.readLine();

      for(int l = 0; l < 16; l++)
         letters[i].f[l] /= 15.f;

      int symbolIdx = characterSymbol - 'A';
      letters[i].symbolIdx = symbolIdx;
      letters[i].outputs[symbolIdx] = 1;
      counterForLetter[symbolIdx]++;

      // Display the counted number for each character
      if(i == NUMBER_OF_PATTERNS-1){
         msg.clear();

         for(symbolIdx = 0; symbolIdx < OUTPUT_NEURONS; symbolIdx++) {
            lineOfData.sprintf("number of patterns for Letter %c = %d\n", 'A' + symbolIdx, counterForLetter[symbolIdx]);
            msg.append(lineOfData);
         }

         ui->plainTextEdit_results->setPlainText(msg);
         qApp->processEvents();
      }

      i++;
   }

   msg.append("done.");
   ui->plainTextEdit_results->setPlainText(msg);

   qApp->processEvents();

   patternsLoadedFromFile=true;

}

void MainWindow::on_horizScrollBar_LearningRate_valueChanged(int value)
{
    ui->lcdNumber_LearningRate->setSegmentStyle(QLCDNumber::Filled);
    ui->lcdNumber_LearningRate->display(value/1000.0);
    LEARNING_RATE = value/1000.0;
}

void MainWindow::on_pushButton_Classify_Test_Pattern_clicked()
{
    char characterSymbol, t;
    QString q(ui->plainTextEdit_Input_Pattern->toPlainText());
    double* classificationResults;

    QTextStream line(&q);

    memset(&testPattern, 0, sizeof(LetterStructure));
    line >> characterSymbol >> t >> testPattern.f[0] >> t >>  testPattern.f[1] >> t >>  testPattern.f[2] >> t >>  testPattern.f[3] >> t >>  testPattern.f[4] >> t >>  testPattern.f[5] >> t >>  testPattern.f[6] >> t >>  testPattern.f[7] >> t >>  testPattern.f[8] >> t >>  testPattern.f[9] >> t >>  testPattern.f[10] >> t >>  testPattern.f[11] >> t >> testPattern.f[12] >> t >> testPattern.f[13] >> t >> testPattern.f[14] >> t >> testPattern.f[15];

    for(int l = 0; l < 16; l++)
       testPattern.f[l] /= 15.f;

    int symbolIdx = characterSymbol - 'A';
    testPattern.symbolIdx = symbolIdx;
    testPattern.outputs[symbolIdx] = 1;

    //---------------------------------
    classificationResults = bp->testNetwork(testPattern);
    int resultOutput = bp->action(classificationResults);
    ui->lcdNumber_Output->display(resultOutput);
    ui->label_Output->setText(QString('A' + resultOutput));

     QString textClassification;
     textClassification.sprintf("letter %c", 'A' + symbolIdx);

    if (bp->action(classificationResults) == symbolIdx) {
        qDebug() << "correct classification.";
        ui->label_Classification->setText(textClassification + ", - Correct classification!");
    } else {
        qDebug() << "incorrect classification.";
        ui->label_Classification->setText(textClassification + ", -XXX- Incorrect classification.");
    }
}

void MainWindow::on_pushButton_Train_Network_Max_Epochs_clicked()
{
   QString msg;

   if(!patternsLoadedFromFile)
   {
      msg.clear();
      msg.append("\nMissing training patterns.  Load data set first.\n");
      ui->plainTextEdit_results->setPlainText(msg);
      return;
   }

   MAX_EPOCHS = ui->spinBox_training_Epochs->value();

   // Req: trains the network continuously until either the maximum epochs has been reached.
   for(int i=0; i < MAX_EPOCHS; i++)
   {
      msg.clear();
      msg.append("\nTraining in progress...\n");
      msg.append("\nEpoch=");
      msg.append(QString::number(i));
      ui->plainTextEdit_results->setPlainText(msg);
      qApp->processEvents();

      bp->trainNetwork(1);
      double goodClassification = bp->getGoodClassificationPercent();

      if(i % 2) {
         ui->lcdNumber_SSE->display(bp->getError_SSE());
         ui->lcdNumber_percentageOfGoodClassification->display(goodClassification);
      }

      qApp->processEvents();

      // Req: trains the network continuously until the target percentage of good classification has been met.
      if(goodClassification >= TARGET_GOOD_CLAASIFICATION) {
         qDebug() << "Current percentage of Good classification is over the target value. So, finish the training. ";
      }

      update();

      // Req: For every multiple of 10 epochs, save all weights into a file.
      if(saveFile) {
         //if((i % 10 == 0) || (i == MAX_EPOCHS - 1)) {
         bool isLast = ((i == MAX_EPOCHS - 1) || (goodClassification >= TARGET_GOOD_CLAASIFICATION)) ;

         bp->saveTrainStatus(isLast);

         if(isLast) {
            bp->saveWeights(ui->plainTextEdit_saveWeightsAs->toPlainText());
            ui->plainTextEdit_results->setPlainText("Weights saved into file.");
            bp->saveConfusionMatrix();
            break;
         }
      }

      qApp->processEvents();
   }
}

void MainWindow::on_pushButton_Initialise_Network_clicked()
{
    bp->initialise();

    QString msg;
    msg.append("Network is initialized.\n");
    ui->plainTextEdit_results->setPlainText(msg);
}

void MainWindow::on_pushButton_Test_All_Patterns_clicked()
{
   int      i = 0;

   bp->initTest();

   for(i=NUMBER_OF_TRAINING_PATTERNS; i < NUMBER_OF_PATTERNS; i++)
   {
      bp->testNetwork(letters[i]);
   }

   ui->lcdNumber_SSE->display(bp->getError_SSE());
   ui->lcdNumber_percentageOfGoodClassification->display(bp->getGoodClassificationPercent());
   qDebug() << bp->getError_MSE() << bp->getError_SSE() << bp->getGoodClassificationPercent();
}

void MainWindow::on_pushButton_testNetOnTrainingSet_clicked()
{
   bp->initTest();

   for(int i=0; i < NUMBER_OF_TRAINING_PATTERNS; i++)
   {
      bp->testNetwork(letters[i]);
   }

   ui->lcdNumber_SSE->display(bp->getError_SSE());
   ui->lcdNumber_percentageOfGoodClassification->display(bp->getGoodClassificationPercent());
   qDebug() << bp->getError_MSE() << bp->getError_SSE() << bp->getGoodClassificationPercent();
}

void MainWindow::on_pushButton_Save_Weights_clicked()
{
   bp->saveWeights(ui->plainTextEdit_saveWeightsAs->toPlainText());

   QString msg;
   QString lineOfText;

   lineOfText = "weights saved to file: " + ui->plainTextEdit_saveWeightsAs->toPlainText();

   msg.append(lineOfText);

   ui->plainTextEdit_results->setPlainText(msg);
}

void MainWindow::on_pushButton_Load_Weights_clicked()
{
   bp->loadWeights(ui->plainTextEdit_fileNameLoadWeights->toPlainText());

   QString msg;

   msg.append("weights loaded.\n");

   ui->plainTextEdit_results->setPlainText(msg);
}

void MainWindow::on_horizScrollBar_LearningRate_actionTriggered(int action)
{

}

// Req:	Provide a facility for shuffling data before feeding it to the network during training
void MainWindow::on_pushButton_Shuffle_Data_clicked()
{
   QString msg;
   msg.append("Start shuffling.\n");
   ui->plainTextEdit_results->setPlainText(msg);

   std::random_shuffle(std::begin(letters), std::end(letters));

   msg.append("Shuffling is finished. \n");
   ui->plainTextEdit_results->setPlainText(msg);

   for(int t = 0; t < 10; t++)
      qDebug() << "Check shuffling: (" << ('A' + letters[t].symbolIdx) << ") " << letters[t].f[0] << letters[t].f[1] << letters[t].f[2];

   msg.append("done.");
   ui->plainTextEdit_results->setPlainText(msg);

   qApp->processEvents();
}
